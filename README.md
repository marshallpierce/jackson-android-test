A starting point for running Jackson tests inside Android to test compatibility.

To build:

```
./gradlew clean build
```

Due to a bug in the sdk manager plugin, the first time it tries to download the android sdk it won't succeed. Run the build again and it should succeed.

To run tests on a device:

```
./gradlew connectedDebugAndroidTest
```

You can create an emulator with `android avd`. The `android` command will be in the `tools` subdir of your Android SDK install. If you let the gradle build download the sdk for you, this will be in `~/.android-sdk`.
