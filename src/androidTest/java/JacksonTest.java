import android.support.test.runner.AndroidJUnit4;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)

public final class JacksonTest {

    @Test
    public void testSerializeEmptyList() throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        ArrayList<String> list = new ArrayList<>();
        assertEquals("[]", mapper.writeValueAsString(list));
    }
}
